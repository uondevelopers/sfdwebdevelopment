function Timeline() {
    
    this.events = [
        {
            date: new Date(2017,3,5),
            title: 'ASIAN BUSINESS ASSOCIATION SUMMER RECEPTION 2017',
            img: 'https://images.pexels.com/photos/414612/pexels-photo-414612.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
            desc: '(While Doc isn\'t looking Marty slips the letter into the pocket of his jacket, then he leaves to pick up Lorraine.) (School Parking Lot) (Marty and Lorraine arrive at the dance. Marty\'s very nervous.) Do you mind if we park for a while? That\'s a great idea. I\'d love to park.',
            page: './past_work_template.html'
        },
        {
            date: new Date(1999,7,8),
            title: 'BRITISH BUSINESS ASSOCIATION SUMMER RECEPTION 1999',
            img: 'https://stepfreedance.com/assets/img/ourwork/Fola.png',
            desc: '(Closet) (Jennifer, still hiding in the closet, listens to them.) (o.s) Your father\'s biggest problem Marlene is that he loses all self control when someone calls him chicken. How many times have we heard it George? (Kitchen) Mom...',
            page: './past_work_template.html'
        },
        {
            date: new Date(1886,5,6),
            title: 'AFRICAN BUSINESS ASSOCIATION SUMMER RECEPTION 1886',
            img: 'https://stepfreedance.com/assets/img/ourwork/Tandav.png',
            desc: 'Take a look at what just breezed in the door. Why I didn\'t know the circus was in town! Looks like he got that shirt off\'n a dead Chinee. (The others laugh) (to Marty) What\'ll it be, stranger?',
            page: './past_work_template.html'
        },
        {
            date: new Date(1655,3,11),
            title: 'SPANISH BUSINESS ASSOCIATION SUMMER RECEPTION 1655',
            img: 'https://stepfreedance.com/assets/img/ourwork/ABA.png',
            desc: '(While Doc isn\'t looking Marty slips the letter into the pocket of his jacket, then he leaves to pick up Lorraine.) (School Parking Lot) (Marty and Lorraine arrive at the dance. Marty\'s very nervous.) Do you mind if we park for a while? That\'s a great idea. I\'d love to park.(Later) (Marty is reading the letter aloud and Doc is looking at the circuit control microchip though a magnifying glass.) "...As you can see, the lightning bolt shorted out the time circuit control microchip. The attached she-she..." Schematic. "...schematic diagram will allow you to build a replacement unit with the 1955 components, thus restoring the time machine to perfect working order."',
            page: './past_work_template.html'
        }
    ];
    
    this.populateTimeline();
    
    var my_posts = $("[rel=tooltip]");

	var size = $(window).width();
	for(i=0;i<my_posts.length;i++){
		the_post = $(my_posts[i]);

		if(the_post.hasClass('invert') && size >=767 ){
			the_post.tooltip({ placement: 'left'});
			the_post.css("cursor","pointer");
		}else{
			the_post.tooltip({ placement: 'right'});
			the_post.css("cursor","pointer");
		}
	}
}

Timeline.prototype.populateTimeline = function(){
    var count = 0;
    this.events.forEach(function(e){
        
        var cardText = ((count%2 == 0)?"<li>":"<li class=\"timeline-inverted\">")+'<div class="timeline-badge primary"><a><i class="far fa-dot-circle'+((count%2 == 0)?"":" invert")+'" rel="tooltip" title="'+e.date.toDateString()+'" id=""></i></a></div><div class="timeline-panel"><div class="timeline-heading"><h2>'+e.title+'</h2><img class="img-responsive" src="'+e.img+'"></div><div class="timeline-body"><p>'+e.desc+'</p></div><div class="timeline-footer"><a><i class="far fa-thumbs-up"></i>  </a><a><i class="fas fa-share-alt"></i></a><a class="text-danger pull-right" href="'+e.page+'">Continue reading</a></div></div></li>';
        
//        var cardText = '<div class="timeline-card-container '+((count%2==0)?"t-left":"t-right")+'"><div class="timeline-date">'+e.date.toDateString()+'</div><div class="timeline-card"><div class="timeline-card-pic" style="height: 30%; width: 100%; background-color: #cc0000; margin-bottom: 10px;"></div><h5>'+e.title+'</h5><p>'+e.desc+'</p></div></div>';
        console.log(cardText);
        $(".timeline").html($(".timeline").html() + cardText);
        count++;
    });
    $(".timeline").html($(".timeline").html() + '<li class="clearfix" style="float: none;"></li>');
}

$(function(){
  var btn_share = $("[rel=modalShare]");
  
  btn_share.click(function(e){
    e.preventDefault();
    $("#myModal").modal('show');
  });
});

var timeline = new Timeline();