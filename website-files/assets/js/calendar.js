function CalendarApp(date) {

    if (!(date instanceof Date)) {
        date = new Date();
    }

    this.days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    this.months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

    // IMPORTANT - THIS ARRAY SHOULD CONTAIN EVENTS
    /*
    Eventbrite pulls should be stored in this array in this sort of format - pop me a message if the format/design need to be changed to fit what exactly gets served from the API
    */
    this.apts = [
        {
            name: 'Test',
            day: new Date(2019, 4, 7),
            title: 'Step Free Dance - Cosmosphere',
            img: 'https://stepfreedance.com/assets/img/ourwork/ABA.png',
            desc: 'This is the desc for our first huge family event: Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lectus proin nibh nisl condimentum id. Iaculis urna id volutpat lacus laoreet. Maecenas accumsan lacus vel facilisis volutpat est velit egestas. Viverra accumsan in nisl nisi scelerisque eu ultrices. Arcu dui vivamus arcu felis bibendum ut tristique et egestas. Dui sapien eget mi proin sed libero enim sed faucibus. Urna porttitor rhoncus dolor purus non. Netus et malesuada fames ac turpis. Ut etiam sit amet nisl purus in mollis nunc sed. Blandit volutpat maecenas volutpat blandit aliquam.',
            info: 'This is the info for the most amazing event this side of the cosmosphere',
            prices: [{
                price: '£8.50',
                label: 'adult'
            }, {
                price: '£4.30',
                label: 'child'
            }]
        },

        {
            name: 'Test 2',
            day: new Date(2019, 4, 9),
            title: 'Step Free Dance - Astrosphere',
            img: 'https://stepfreedance.com/assets/img/ourwork/Tandav.png',
            desc: 'This is the desc for our second huge family event: Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lectus proin nibh nisl condimentum id. Iaculis urna id volutpat lacus laoreet. Maecenas accumsan lacus vel facilisis volutpat est velit egestas. Viverra accumsan in nisl nisi scelerisque eu ultrices. Arcu dui vivamus arcu felis bibendum ut tristique et egestas. Dui sapien eget mi proin sed libero enim sed faucibus. Urna porttitor rhoncus dolor purus non. Netus et malesuada fames ac turpis. Ut etiam sit amet nisl purus in mollis nunc sed. Blandit volutpat maecenas volutpat blandit aliquam.',
            info: 'This is the info for the second most amazing event this side of the astrosphere',
            prices: [{
                price: '£7.80',
                label: 'adult'
            }, {
                price: '£1.00',
                label: 'child'
            }, {
                price: '£3.20',
                label: 'concession'
            }]
        },

        {
            name: 'Test 3',
            day: new Date(2019, 4, 17),
            title: 'Step Free Dance - Tritusphere',
            img: 'https://stepfreedance.com/assets/img/ourwork/Fola.png',
            desc: 'This is the desc for our third huge family event: Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lectus proin nibh nisl condimentum id. Iaculis urna id volutpat lacus laoreet. Maecenas accumsan lacus vel facilisis volutpat est velit egestas. Viverra accumsan in nisl nisi scelerisque eu ultrices. Arcu dui vivamus arcu felis bibendum ut tristique et egestas. Dui sapien eget mi proin sed libero enim sed faucibus. Urna porttitor rhoncus dolor purus non. Netus et malesuada fames ac turpis. Ut etiam sit amet nisl purus in mollis nunc sed. Blandit volutpat maecenas volutpat blandit aliquam.',
            info: 'This is the info for the third most amazing event this side of the tritusphere',
            prices: [{
                price: '£4.50',
                label: 'adult'
            }, {
                price: '£1.00',
                label: 'concession'
            }]
        },

        {
            name: 'Test 4',
            day: new Date(2019, 4, 24),
            title: 'Step Free Dance - Dingosphere',
            img: 'https://images.pexels.com/photos/414612/pexels-photo-414612.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
            desc: 'This is the desc for our fourth huge family event: Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lectus proin nibh nisl condimentum id. Iaculis urna id volutpat lacus laoreet. Maecenas accumsan lacus vel facilisis volutpat est velit egestas. Viverra accumsan in nisl nisi scelerisque eu ultrices. Arcu dui vivamus arcu felis bibendum ut tristique et egestas. Dui sapien eget mi proin sed libero enim sed faucibus. Urna porttitor rhoncus dolor purus non. Netus et malesuada fames ac turpis. Ut etiam sit amet nisl purus in mollis nunc sed. Blandit volutpat maecenas volutpat blandit aliquam.',
            info: 'This is the info for the fourth most amazing event this side of the dingosphere',
            prices: [{
                price: '£3.80',
                label: 'adult'
            }, {
                price: '£0.50',
                label: 'child'
            }, {
                price: '£1.20',
                label: 'concession'
            }]
        },
        
        {
            name: 'Test 5',
            day: new Date(2019, 4, 26),
            title: 'Step Free Dance - Big family event',
            img: 'https://images.pexels.com/photos/414612/pexels-photo-414612.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
            desc: 'This is the desc for our fourth huge family event: Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lectus proin nibh nisl condimentum id. Iaculis urna id volutpat lacus laoreet. Maecenas accumsan lacus vel facilisis volutpat est velit egestas. Viverra accumsan in nisl nisi scelerisque eu ultrices. Arcu dui vivamus arcu felis bibendum ut tristique et egestas. Dui sapien eget mi proin sed libero enim sed faucibus. Urna porttitor rhoncus dolor purus non. Netus et malesuada fames ac turpis. Ut etiam sit amet nisl purus in mollis nunc sed. Blandit volutpat maecenas volutpat blandit aliquam.',
            info: 'This is the info for the fourth most amazing event this side of the dingosphere',
            prices: [{
                price: '£3.80',
                label: 'adult'
            }, {
                price: '£0.50',
                label: 'child'
            }, {
                price: '£1.20',
                label: 'concession'
            }]
        },

    ];


    this.eles = {};
    this.calDaySelected = null;

    this.calendar = document.getElementById("calendar-app");

    this.calendarView = document.getElementById("dates");

    this.calendarMonthDiv = document.getElementById("calendar-month");
    this.calendarMonthLastDiv = document.getElementById("calendar-month-last");
    this.calendarMonthNextDiv = document.getElementById("calendar-month-next");

    this.dayInspirationalQuote = document.getElementById("inspirational-quote");

    this.todayIsSpan = document.getElementById("footer-date");
    // this.eventsCountSpan = document.getElementById("footer-events");
    this.dayViewEle = document.getElementById("day-view");
    this.dayViewExitEle = document.getElementById("day-view-exit");
    this.dayViewDateEle = document.getElementById("day-view-date");
    this.addDayEventEle = document.getElementById("add-event");
    this.dayEventsEle = document.getElementById("day-events");
    
    this.dayWindowOpen = false;

    // EVENT ELEMENTS
    this.dayEventImage = document.getElementById("event-img");
    this.dayEventDesc = document.getElementById("event-desc");
    this.dayEventInfo = document.getElementById("event-info");
    this.dayEventPrices = document.getElementById("event-prices-container");
    this.featuredEventPane = document.getElementById("featured-event-pane");
    this.maxFeaturedEvents = 2;
    this.featuredEventImages = [];
    this.pList = [];

    //    this.dayEventAddForm = {
    //        cancelBtn: document.getElementById("add-event-cancel"),
    //        addBtn: document.getElementById("add-event-save"),
    //        nameEvent: document.getElementById("input-add-event-name"),
    //        startTime: document.getElementById("input-add-event-start-time"),
    //        endTime: document.getElementById("input-add-event-end-time"),
    //        startAMPM: document.getElementById("input-add-event-start-ampm"),
    //        endAMPM: document.getElementById("input-add-event-end-ampm")
    //    };
    this.dayEventsList = document.getElementById("day-events-list");
    this.dayEventBoxEle = document.getElementById("add-day-event-box");

    /* Start the app */
    this.showView(date);
    this.updateFeaturedEvents()
    this.addEventListeners();
    this.todayIsSpan.textContent = "Today is " + this.months[date.getMonth()] + " " + date.getDate();
}



/* ========================================================== */
/* ============= IMPORTANT AND CUSTOM FUNCTIONS ============= */
/* ========================================================== */

// Update the featured events pane with the [n] first featured events
CalendarApp.prototype.updateFeaturedEvents = function () {

    // Find the two (could be more or less later) most recent events
    var featured_events = this.apts.sort(function (a, b) {
        return a.day - b.day;
    }).slice(0, this.maxFeaturedEvents);

    // Place the info into the featured event panes
    this.featuredEventPane.innerHTML = "";
    for (var i = 0; i < this.maxFeaturedEvents; i++) {
        this.featuredEventPane.innerHTML += "<div class='jumbotron container featured-event-box' id='featured-event-box'><div id='loader'></div><div class='featured-info' id='featured-info' style='opacity:0;'><h2>" + featured_events[i].title + "</h2><button type='button' class='featured-event-btn btn btn-more' onclick='myFunction(\"" + featured_events[i].day.toString() + "\");'>See event</button><p class='featured-event-desc'>" + featured_events[i].desc + "</p></div><img class='featured-event-img' id='featured-event-img-" + i.toString() + "' src='" + featured_events[i].img + "'></div>";
    }
    Array.from(document.querySelectorAll('#featured-event-box')).forEach(function (el) {
        el.onmouseover = function () {
            el.children[1].style.opacity = '1';
        };
    });
};

var myFunction = function (day) {
    //console.log(day);
    calendar.openDayWindow(day, calendar.getEvent(day));
}

// To check if a day has an event
CalendarApp.prototype.hasEvent = function (day) {

    var event = null;

    this.apts.forEach(function (apt, i) {

        //console.log("Comparing: "+day.toString()+" to event date "+apt.day.toString());

        if (day.toString() == apt.day.toString()) {

            apt.index = i;
            event = apt;
            return;
        }
    });

    if (event) {
        return true;
    }
    return false;
};

// To get the event values for the event on a particular day
CalendarApp.prototype.getEvent = function (day) {

    var event = null;

    this.apts.forEach(function (apt, i) {

        //console.log("Comparing: "+day.toString()+" to event date "+apt.day.toString());

        if (day.toString() == apt.day.toString()) {

            apt.index = i;
            event = apt;
            return;
        }
    });

    return event;
};


/*
 *
 * THIS FUNCTION CONTROLS THE CONTENT OF THE DAY EVENT WINDOW
 *
 */
CalendarApp.prototype.openDayWindow = function (date, event) {

    var now = new Date();
    var day = new Date(date);
    this.dayViewDateEle.textContent = this.days[day.getDay()] + ", " + this.months[day.getMonth()] + " " + day.getDate() + ", " + day.getFullYear();
    this.dayViewDateEle.setAttribute('data-date', day);
    this.dayViewEle.classList.add("calendar--day-view-active");

    this.dayEventsEle.textContent = event.title;
    this.dayEventImage.src = event.img;
    this.dayEventDesc.textContent = event.desc;
    this.dayEventInfo.textContent = event.info;

    var prices = '';
    event.prices.forEach(function (price_item) {
        prices += '<span class="event-price">' + price_item.price + " - " + price_item.label + '</span>' + '<br>';
    });
    this.dayEventPrices.innerHTML = prices.slice(0, prices.length - 4);
    
    
    if (this.dayWindowOpen) {
        $('html, body').animate({
            'scrollTop' : ($(this.dayViewEle).offset().top - 80)
        });
    } else {
        $('html, body').animate({
            'scrollTop' : ($(this.dayViewEle).offset().top - 140)
        });
    }
    
    this.dayWindowOpen = true;

};

CalendarApp.prototype.showNewMonth = function (e) {

    var date = e.currentTarget.dataset.date;
    var newMonthDate = new Date(date);

    var now = new Date();
    //console.log((now.getMonth() - newMonthDate.getMonth()));

    // this if statement stops you from navigating to previous months before the current
    if ((now.getMonth() - newMonthDate.getMonth() <= 0) || (now.getFullYear() != newMonthDate.getFullYear())) {
        this.showView(newMonthDate);
        this.closeDayWindow();
        return true;
    } else {
        return false;
    }
};

CalendarApp.prototype.showView = function (date) {
    if (!date || (!(date instanceof Date))) date = new Date();
    var now = new Date(date),
        y = now.getFullYear(),
        m = now.getMonth();
    var today = new Date();

    var lastDayOfM = new Date(y, m + 1, 0).getDate();
    var startingD = new Date(y, m, 1).getDay();
    var lastM = new Date(y, now.getMonth() - 1, 1);
    var nextM = new Date(y, now.getMonth() + 1, 1);

    this.calendarMonthDiv.classList.remove("cview__month-activate");
    this.calendarMonthDiv.classList.add("cview__month-reset");

    while (this.calendarView.firstChild) {
        this.calendarView.removeChild(this.calendarView.firstChild);
    }

    // build up spacers
    for (var x = 0; x < startingD; x++) {
        var spacer = document.createElement("div");
        spacer.className = "cview--spacer";
        this.calendarView.appendChild(spacer);
    }

    for (var z = 1; z <= lastDayOfM; z++) {

        var _date = new Date(y, m, z);
        var day = document.createElement("div");
        day.className = "cview--date";
        day.textContent = z;
        day.setAttribute("data-date", _date);
        day.onclick = this.showDay.bind(this);

        // check if todays date
        if (z == today.getDate() && y == today.getFullYear() && m == today.getMonth()) {
            day.classList.add("today");
        } else if (z < today.getDate() && m <= today.getMonth() && y <= today.getFullYear()) {
            day.classList.add("past");
        }

        // check if has events to show (legacy)
        //        if (this.aptDates.indexOf(_date.toString()) !== -1) {
        //            day.classList.add("has-events");
        //        }

        // check if has events to show (custom)
        if (this.hasEvent(_date)) {
            day.classList.add("has-events");
        }

        this.calendarView.appendChild(day);
    }

    var _that = this;
    setTimeout(function () {
        _that.calendarMonthDiv.classList.add("cview__month-activate");
    }, 50);

    this.calendarMonthDiv.textContent = this.months[now.getMonth()] + " " + now.getFullYear();
    this.calendarMonthDiv.setAttribute("data-date", now);




    var comp = new Date(lastM);

    comp.setTime(today.getTime());
    comp.setMonth(lastM.getMonth());
    comp.setFullYear(lastM.getFullYear());

    //    console.log(today);
    //    console.log(comp);
    //    console.log(today - comp);


    if (today - comp <= 0) {
        this.calendarMonthLastDiv.innerHTML = " <span style=\"font-weight: 800; font-size: 18px; font-family: 'Avenir', sans-serif;\">← </span>" + this.months[lastM.getMonth()];
    } else {
        //this.calendarMonthLastDiv.textContent = this.months[lastM.getMonth()];
        this.calendarMonthLastDiv.textContent = "";
    }
    this.calendarMonthLastDiv.setAttribute("data-date", lastM);


    this.calendarMonthNextDiv.innerHTML = this.months[nextM.getMonth()] + " <span style=\"font-weight: 800; font-size: 18px; font-family: 'Avenir', sans-serif;\"> →</span>";
    this.calendarMonthNextDiv.setAttribute("data-date", nextM);

};
/* ========================================================== */
/* =================== END OF CUSTOM STUFF ================== */
/* ========================================================== */


CalendarApp.prototype.addEventListeners = function () {

    this.calendar.addEventListener("click", this.mainCalendarClickClose.bind(this));
    this.todayIsSpan.addEventListener("click", this.showView.bind(this));
    this.calendarMonthLastDiv.addEventListener("click", this.showNewMonth.bind(this));
    this.calendarMonthNextDiv.addEventListener("click", this.showNewMonth.bind(this));
    this.dayViewExitEle.addEventListener("click", this.closeDayWindow.bind(this));
    this.dayViewDateEle.addEventListener("click", this.showNewMonth.bind(this));

};


CalendarApp.prototype.showDay = function (e, dayEle) {
    e.stopPropagation();
    if (!dayEle) {
        dayEle = e.currentTarget;
    }
    var dayDate = new Date(dayEle.getAttribute('data-date'));

    this.calDaySelected = dayEle;

    if (this.hasEvent(dayDate)) {
        this.openDayWindow(dayDate, this.getEvent(dayDate));
    }
};

CalendarApp.prototype.showEventsCreateElesView = function (events) {
    var ul = document.createElement("ul");
    ul.className = 'day-event-list-ul';
    events = this.sortEventsByTime(events);
    var _this = this;
    events.forEach(function (event) {
        var _start = new Date(event.startTime);
        var _end = new Date(event.endTime);
        var idx = event.index;
        var li = document.createElement("li");
        li.className = "event-dates";
        // li.innerHtml
        var html = "<span class='start-time'>" + _start.toLocaleTimeString(navigator.language, {
            hour: '2-digit',
            minute: '2-digit'
        }) + "</span> <small>through</small> ";
        html += "<span class='end-time'>" + _end.toLocaleTimeString(navigator.language, {
            hour: '2-digit',
            minute: '2-digit'
        }) + ((_end.getDate() != _start.getDate()) ? ' <small>on ' + _end.toLocaleDateString() + "</small>" : '') + "</span>";


        html += "<span class='event-name'>" + event.name + "</span>";

        var div = document.createElement("div");
        div.className = "event-dates";
        div.innerHTML = html;

        var deleteBtn = document.createElement("span");
        var deleteText = document.createTextNode("delete");
        deleteBtn.className = "event-delete";
        deleteBtn.setAttribute("data-idx", idx);
        deleteBtn.appendChild(deleteText);
        deleteBtn.onclick = _this.deleteEvent.bind(_this);

        div.appendChild(deleteBtn);

        li.appendChild(div);
        ul.appendChild(li);
    });
    return ul;
};

CalendarApp.prototype.sortEventsByTime = function (events) {
    if (!events) return [];
    return events.sort(function compare(a, b) {
        if (new Date(a.startTime) < new Date(b.startTime)) {
            return -1;
        }
        if (new Date(a.startTime) > new Date(b.startTime)) {
            return 1;
        }
        // a must be equal to b
        return 0;
    });
};

CalendarApp.prototype.closeDayWindow = function () {
    this.dayWindowOpen = false;
    this.dayViewEle.classList.remove("calendar--day-view-active");
    this.closeNewEventBox();
};

CalendarApp.prototype.mainCalendarClickClose = function (e) {
    if (e.currentTarget != e.target) {
        return;
    }

    this.dayViewEle.classList.remove("calendar--day-view-active");
    this.closeNewEventBox();
};

CalendarApp.prototype.closeNewEventBox = function (e) {

    if (e && e.keyCode && e.keyCode != 13) return false;

};

var calendar = new CalendarApp();
//console.log(calendar);
